
var MyStack = (function() {
	var myArray = [];
	function MyStack() {
		'use strict';
		console.log("MyStack Constructor called.");
		
		//defaultArray();
		//getFrontElement();
		//getBackElement();
		//$("#qCount").html("Number items in the queue is: " + myArray.length);
	};
	
	MyStack.prototype.getStackLength = function(){
	'use strict';
		//var element = myArray[0];
		//$("#qFront").html("Front Element of myArray is: " + myArray[0]);
		return myArray.length;
	};
		
	MyStack.prototype.pushStack = function(element){
	'use strict';
		return myArray.push(element)
	};
	
	MyStack.prototype.popStack = function(){
	'use strict';
		return myArray.pop();
		
	};
	
	MyStack.prototype.removeItem = function(element){
	'use strict';
		var elementIndex = myArray.indexOf(element);
		if ((elementIndex == myArray.length - 1) || (elementIndex == 0)){
			return false;
		};
		
		adjustStack(elementIndex);
		return true;
	};
	
	//remove element at startPos and move up the elements by one
	var adjustStack = function(startPos){
		var endPos = myArray.length - 1;
		
		for(; startPos < endPos; startPos++){
			myArray[startPos] = myArray[startPos + 1]
		};
		
		// remove the last element in the array
		myArray.pop();

	};
	return MyStack;
}());

var MyQueue = (function() {
	'use strict';
	
	var myArray = [];
	function MyQueue() {
		'use strict';
		console.log("MyQueue Constructor called.");
		
		//defaultArray();
		//getFrontElement();
		//getBackElement();
		//$("#qCount").html("Number items in the queue is: " + myArray.length);
	};
	
	var defaultArray = function(){
	'use strict';
		myArray.push("alpha");
		myArray.push("bravo");
		myArray.push("Charlie");
		
		$("#qCount").html("Number items in the queue is: " + myArray.length);
};

	MyQueue.prototype.getQueueLength = function(){
	'use strict';
		var element = myArray[0];
		//$("#qFront").html("Front Element of myArray is: " + myArray[0]);
		return myArray.length;
};
	MyQueue.prototype.getFrontElement = function(){
	'use strict';
		var element = myArray[0];
		//$("#qFront").html("Front Element of myArray is: " + myArray[0]);
		return myArray[0];
};

	 MyQueue.prototype.getBackElement = function(){
	 'use strict';
		var element = myArray[myArray.length - 1];
		//$("#qBack").html("Back Element of myArray is: " + myArray[myArray.length - 1]);
		return myArray[myArray.length - 1];
};

	MyQueue.prototype.enQueue = function(element){
	'use strict';
		var retVal = false;
		if (myArray.push(element) > 0 );
			retVal = true;
		return retVal;
};
	MyQueue.prototype.deQueue = function(){
	'use strict';
		//return myArray.pop(); // does not work returns the last element in the array.
		return myArray.shift();
		
};

	MyQueue.prototype.padNumber = function(numberToPad, width, padValue) {
        'use strict';
        padValue = padValue || '0';
        numberToPad = numberToPad + '';
        if (numberToPad.length >= width) {
            return numberToPad;
        } else {
            return new Array(width - numberToPad.length + 1).join(padValue) + numberToPad;
        }
    };
	
	return MyQueue;
}());

$(document).ready(function() {
    var myQueue = new MyQueue();
	var queueElement;
	var qLength = 0;
/*	
	myQueue.enQueue("alpha");
	myQueue.enQueue("bravo ");
	myQueue.enQueue("charlie");
	$("#qFront").html("Front Element of myQueue is: " + myQueue.getFrontElement());
	$("#qBack").html("Back Element of myQueue is: " + myQueue.getBackElement());
	$("#qCount").html("Number of elements in myQueue is: " + myQueue.getQueueLength());
	

	while (myQueue.getQueueLength() > 0){
		qLength++;
		queueElement = myQueue.deQueue();
	};
	
	myQueue.enQueue("alpha");
	myQueue.enQueue("bravo ");
	myQueue.enQueue("charlie");
	
	qLength = myQueue.getQueueLength();
	for(x = 0; x < qLength; x++){
		queueElement = myQueue.deQueue();
	};
	
	for(x=0; x<100000; x++){
		queueElement = myQueue.padNumber(x+1, 6, 0);
		myQueue.enQueue("Item" + queueElement);
	};
	
	qLength = myQueue.getQueueLength();
*/
	
	var myStack = new MyStack();
	var stackElement;
	var stackLength = 0;
	
	
	stackElement = myStack.popStack();
	
	myStack.pushStack("alpha");
	myStack.pushStack("bravo");
	stackLength = myStack.pushStack("charlie");

	stackElement = myStack.popStack();
	
	myStack.pushStack("charlie");
	myStack.pushStack("delta");
	myStack.pushStack("echo");
	
	myStack.removeItem("bravo");
	myStack.removeItem("alpha");
	myStack.removeItem("echo");
  
});;