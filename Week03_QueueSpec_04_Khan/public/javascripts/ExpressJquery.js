var MyQueue = (function() {
	'use strict';
	
	var myArray = [];
	function MyQueue() {
		'use strict';
		console.log("MyQueue Constructor called.");
		
		//defaultArray();
		//getFrontElement();
		//getBackElement();
		//$("#qCount").html("Number items in the queue is: " + myArray.length);
	};
	
	var defaultArray = function(){
	'use strict';
		myArray.push("alpha");
		myArray.push("bravo");
		myArray.push("Charlie");
		
		$("#qCount").html("Number items in the queue is: " + myArray.length);
};

	MyQueue.prototype.getQueueLength = function(){
	'use strict';
		var element = myArray[0];
		//$("#qFront").html("Front Element of myArray is: " + myArray[0]);
		return myArray.length;
};
	MyQueue.prototype.getFrontElement = function(){
	'use strict';
		var element = myArray[0];
		//$("#qFront").html("Front Element of myArray is: " + myArray[0]);
		return myArray[0];
};

	 MyQueue.prototype.getBackElement = function(){
	 'use strict';
		var element = myArray[myArray.length - 1];
		//$("#qBack").html("Back Element of myArray is: " + myArray[myArray.length - 1]);
		return myArray[myArray.length - 1];
};

	MyQueue.prototype.enQueue = function(element){
	'use strict';
		var retVal = false;
		if (myArray.push(element) > 0 );
			retVal = true;
		return retVal;
};
	MyQueue.prototype.deQueue = function(){
	'use strict';
		//return myArray.pop(); // does not work returns the last element in the array.
		return myArray.shift();
		
};

	MyQueue.prototype.padNumber = function(numberToPad, width, padValue) {
        'use strict';
        padValue = padValue || '0';
        numberToPad = numberToPad + '';
        if (numberToPad.length >= width) {
            return numberToPad;
        } else {
            return new Array(width - numberToPad.length + 1).join(padValue) + numberToPad;
        }
    };
	
	return MyQueue;
}());

$(document).ready(function() {
    var myQueue = new MyQueue();
	var queueElement;
	myQueue.enQueue("alpha");
	myQueue.enQueue("bravo ");
	myQueue.enQueue("charlie");
	$("#qFront").html("Front Element of myQueue is: " + myQueue.getFrontElement());
	$("#qBack").html("Back Element of myQueue is: " + myQueue.getBackElement());
	$("#qCount").html("Number of elements in myQueue is: " + myQueue.getQueueLength());
	
	var qLength = 0;
	while (myQueue.getQueueLength() > 0){
		qLength++;
		queueElement = myQueue.deQueue();
	};
	
	myQueue.enQueue("alpha");
	myQueue.enQueue("bravo ");
	myQueue.enQueue("charlie");
	
	qLength = myQueue.getQueueLength();
	for(x = 0; x < qLength; x++){
		queueElement = myQueue.deQueue();
};
	

	
	
  
});;