/*globals describe:true, it:true, expect:true, SailBoatFactory: true, Sloop: true */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(["SailBoatFactory", "Sloop", "Yawl", "Ketch"], function(SailBoatFactory, Sloop, Yawl, Ketch) {'use strict';

// define([], function() {'use strict';
    describe("Simple Factory Suite", function() {
        
        //var SailBoatFactory = require('../Factory/SailBoatFactory');
        //var Sloop = require('../Factory/Sloop');
        //var Yawl = require('../Factory/Yawl'); 

        it("proves we can run a test", function() {
            expect(true).toBe(true);
        });

        it("creates a sloop", function() {
            // Create an instance of our factory that makes boats
            var boatFactory = new SailBoatFactory();
            var sloop = boatFactory.createBoat({
                boatType : "sloop",
                color : "green",
                sails : 3
            });

            // Test to confirm our Sloop was created
            expect( sloop instanceof Sloop).toBe(true);
        });

        it("Tests sloop color is green", function() {
            // Create an instance of our factory that makes boats
            var boatFactory = new SailBoatFactory();
            var sloop = boatFactory.createBoat({
                boatType : "sloop",
                color : "green",
                sails : 3
            });

            expect(sloop.color).toBe('green');
        });
        
        it("Tests sloop has a keel", function() {
            // Create an instance of our factory that makes boats
            var boatFactory = new SailBoatFactory();
            var sloop = boatFactory.createBoat({
                boatType : "sloop",
                color : "green",
                sails : 3
            });

            expect(sloop.keel).toBe(true);
        });
		
		it("creates a yawl", function() {
            // Create an instance of our factory that makes boats
            var boatFactory = new SailBoatFactory();
            var yawl = boatFactory.createBoat({
                boatType : "yawl",
                color : "red",
                sails : 3
            });

            // Test to confirm our Yawl was created
            expect( yawl instanceof Yawl).toBe(true);
        });
        
        it("Tests yawl color is  is red", function() {
            var boatFactory = new SailBoatFactory();
            var yawl = boatFactory.createBoat({
                boatType : "yawl",
                color : "red",
                sails : 3
            });

            expect(yawl.color).toBe('red');
        });
        
        it("Tests yawl has a mizzen", function() {
            var boatFactory = new SailBoatFactory();
            var yawl = boatFactory.createBoat({
                boatType : "yawl",
                color : "red",
                sails : 3
            });

            expect(yawl.mizzen).toBe(true);
        });
        
        it("creates a ketch", function() {
            // Create an instance of our factory that makes boats
            var boatFactory = new SailBoatFactory();
            var ketch = boatFactory.createBoat({
                boatType : "ketch",
                color : "red",
                sails : 3
            });

            // Test to confirm our Yawl was created
            expect( ketch instanceof Ketch).toBe(true);
        });
        
        it("Tests ketch color is  is brown", function() {
            var boatFactory = new SailBoatFactory();
            var ketch = boatFactory.createBoat({
                boatType : "ketch",
                color : "brown",
                sails : 3
            });

            expect(ketch.color).toBe('brown');
        });
        
        it("Tests ketch has a top sail", function() {
            var boatFactory = new SailBoatFactory();
            var ketch = boatFactory.createBoat({
                boatType : "ketch",
                color : "brown",
                sails : 3
            });

            expect(ketch.topsail).toBe(true);
        });
 		
    });

}); 
