

var itemsOrdered = 0;

var productList = ['Poster 1', 'Poster 2', 'Poster 3', 'Poster 4', 'Poster 5', 
				 'Poster 6', 'Poster 7', 'Poster 8', 'Poster 9', 'Poster 10'];
				 
var priceList = [21, 32, 76, 62, 87, 
				 12, 41, 65, 5, 72];				 

function init() {
	setDragables();
	setDroppables();
	$('#orderedImages').children().hide();
	$('.done').hide();
}

//make images draggable
function setDragables() {
	for(var i = 1; i < 11; i++)
	{
		$('#drag' + i).draggable({
			containment: '#checkedImageDisplay',
		})
	}
	
	for(var i = 1; i < 11; i++)
	{
		$('#ordImage' + i).draggable({
			containment: '#orderDispose',
		})
	}
}

//make boxes droppable
function setDroppables() {
 
  $('#checkedImageDisplay').droppable( {
    drop: handleDropEvent
  });
  
  $('#orderDispose').droppable( {
    drop: handleDisposepEvent
  });

}

//deselect ordered iten via order image
//and updeate releted elements
function handleDisposepEvent(event, ui){
  var draggable =  ui.draggable;
  var index = '#' + draggable.attr('value');
  
  $(draggable).toggle( "explode" );
  $(index).attr('checked', false);
  updateOrderedPriceTable();
  var temp = $('#orderedImages').get(0);
  //var tmp = $("#orderedImages:first-child").attr("id")
}


// image has been dropped
function handleDropEvent( event, ui ) {
  var boxID = this.getAttribute('id') ;
  var draggable =  ui.draggable;
  var draggableId = draggable.attr('id');
  var draggableSrc = draggable.attr('src');
  var index = '#' + draggable.attr('value');

  //alert("image source  " + draggableSrc + " and value : " + index );
  showDroppedImage(draggableSrc);
  
  //show dragged item in ordered list of images   
  var id = draggable.attr('value');
  id++;
  showOrderdDiv(id);
  
  //explode and remove the dragged thumbnail
 $(draggable).toggle( "explode" );
 $(draggable).remove();
  
  
  //check the checbox that corresponds with the dragged 
  //image and update the table
  $(index).attr('checked','checked');
  updateOrderedPriceTable();
}

//show image of order tiem with effect
//noice the callback to get it work correctly
function showDroppedImage(scr){
	$("#imageInBox").fadeOut(1000, function(){
		$("#imageInBox").attr("src", scr);
	    imagefadeIn();
	});	
}  

function imagefadeIn(){
	$("#imageInBox").hide().fadeIn(1000);	
}

function showOrderdDiv(id){
	 var temp = '#ordImage' + id;
	 $(temp).show().effect("bounce", 
          {times:3}, 300 );
}

//order check box cllicked
var checkBoxClicked = function(event){
	//var value = this.getAttribute('value') ;
	var index = this.getAttribute('id') ;
	index++;
	
	//update display as just chedked or unchecked
	if($(this).is(':checked')) {
        //alert('checked');
        src = $('#drag' + index).attr('src');
    	showDroppedImage(src);
		showOrderdDiv(index);
    }else{
		//alert('un-checked');
		$('#ordImage' + index).toggle( "explode" );
		$('#ordImage' + index).hide();
	}
	
	updateOrderedPriceTable();
}

//upadate the odered price and total table
var updateOrderedPriceTable = function() {
	var total = 0;
	//clear table except for first row
	$("#heroTbl tr:gt(0)").remove();
	
	//irretrate through Checked boxes
	$("input[type=checkbox]:checked").each(function() {
		var index = ($(this).attr('id')); 
		var product =  productList[index];
		var price =   priceList[index];
		
		total += priceList[index];
		//add row to table
		$("#heroTbl").append('<tr><td>' + product + '</td><td>' + price  + '</td></tr>');
		
	});

	$("#heroTbl").append('<tr><td><strong>Total </strong></td>' + '<td>' + total  + '</td></tr>');
};

var sideClicked = function(){
	//alert("Handler for .click() called.");
	var scrollAmount = $(this).width() - $(this).parent().width();
   	var currentPos = Math.abs(parseInt($(this).css('left')));
    var remainingScroll = scrollAmount - currentPos;
  
    // Scroll half-a-screen by default
   	var nextScroll = Math.floor($(this).parent().width() / 2);
  
   	 // But if there isn't a FULL scroll left, do only the remaining amount.
   	if (remainingScroll < nextScroll) {
    	nextScroll = remainingScroll;
   	}
   
	if (currentPos < scrollAmount) {
	  // Scroll left
	$(this).animate({'left':'-=' + nextScroll}, 'slow');
	}
	else {
	  // Scroll right
	 $(this).animate({'left':'0'}, 'fast');
	}
}

var validateSubmit = function(){
	//get totlal number of checked boxes 
	itemsOrdered = $( "input:checked" ).length;
  	//$( "#debug1" ).text( itemsOrdered + (itemsOrdered === 1 ? " is" : " are") + " checked!" );
	if(itemsOrdered === 0){
		alert('Please Select a Item');
	}
	else{
		$('#photos').remove();
		$('#productCheckBoxes').remove();
		$('#imagesDaggable').remove();
		$('#checkedImageDisplay').remove();
		$('#priceTable').remove();
		$('#orderContainer').remove();
		
		$('.done').show();
		$('.done').append('<h3> Items will be shipped shortly </h3>');
	}
};

	//get totlal number of checked boxes 
	//var n = $( "input:checked" ).length;
  	//$( "#debug1" ).text( n + (n === 1 ? " is" : " are") + " checked!" );
	

	// keep this code
	//irretrate through UnChecked boxes
	/*
	$("input[type=checkbox]:not(:checked)").each(function() {
        alert( $(this).val() );
    });
	*/

$(document).ready(function(){
	
	init();
	
	$("#photos_inner").click(sideClicked);
	$( "input[type=checkbox]" ).on( "click", checkBoxClicked);
	$('#btnButton').click(validateSubmit);
	
	

});
