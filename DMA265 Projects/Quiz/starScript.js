// JavaScript Document
// Mike Khan
// Completed 5/1/2013

//At start it hides all the question <div>s, except for the first question and
//displays the 'html in "section'. 
//When "submit" is clicked for the  first time removeClass() is used to 
//remove "question1" class from 'section' element.
// After that it displays rest of the question one at time using
// prependTo() and empty() until all the questions have been submitted
//Images and questions are aninated when displayed

// Mike Khan
// Added Features Completed 5/19/2013

// buttonSlected() checks if a radio button is checked, if button is checked calls updateScore() 
//if not tells the player to select an answer.
//Keeps score of correct answers that is displayed with animation and a image at the end of quiz

var MAX_QUESTIONS = 10;  // total number of quesions
var counter = 1;		 // tracks number of of question submitted
var clickedButtonPosition = 0;
var correctAnswerCount = 0;
var arryCorrectAnswers = [2,1, 3, 1, 2, 3, 3, 2, 1, 3];


// Checks for the correct answer and updates the score
function updateScore(clickedButtonPosition){
	if(arryCorrectAnswers[counter-1] == clickedButtonPosition) {
		correctAnswerCount++;
		//alert("Number of Answers: " + correctAnswerCount);
	}
}	 // end updateScore()


//user clicked a button
function buttonSlected(){
	
	//get the position of the clicked button
	for (var i = 1; i <= 3; i++){
		var buttonSelector = 'rdButton' + counter + i;
		if (document.getElementById(buttonSelector).checked){
			clickedButtonPosition = i;           //set clicked button position
			break;
		} 
	}
} // end buttonSlected()

function hideClasses(){
	$(".quizComplete").hide();
	// hide all question classe and then display one quesion at time 
	//$(".question1").hide();
	$(".question2").hide();
	$(".question3").hide();
	$(".question4").hide();
	$(".question5").hide();
	$(".question6").hide();
	$(".question7").hide();
	$(".question8").hide();
	$(".question9").hide();
	$(".question10").hide();
}

$(document).ready(function(){
	
	hideClasses();
	
	//show html in 'section' element
	if(counter == 1){
		//alert($('section').html())
	}
	
	//Submit button clicked
	$("#submitButton").click( function(){	
		// check if radio button was selected
		if (clickedButtonPosition === 0){
			alert (" Select an Answer");		// tell the player no selection was  made
			return;								// dont go to the next question.
		} else {
			updateScore(clickedButtonPosition);  // update the player score
			clickedButtonPosition = 0;			 // clear radio button position
		};
		
		// go to the next question
		if(counter == 1){
		$('section').removeClass('question1');
	    }
		
	  	$("section").empty();				
		if(counter <  (MAX_QUESTIONS) ){
			counter++;							
		    $('.question' + counter).prependTo('section'); // using prepentTo() for the exersise, since "section' selector
		    $(".question" + counter).show('slow');			// is empty could use append() as well. animate the display
		}else {
			//all the question sumitted, quiz  finished
			$("#submitButton").hide();
			$("section").empty();
			
			// do some animation wrongAnswers
			//$('<p>this is working</p>').appendTo('.container');
			
			
			$('<p>Wrong = ' + (MAX_QUESTIONS - correctAnswerCount) + '</p>').appendTo('.showResults');
			$('<p>Correct Answers = ' + correctAnswerCount + '</p>').appendTo('.showResults');
			
			$(".quizComplete").show();	
			$(".showResults").animate({padding:"20px",fontSize: "30px"}, 2000);
			

		}
	  }
    );
});
