/*globals describe:true, it:true, expect:true, SailBoatFactory: true, Sloop: true */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(["Sailor", "SailorExpert", "Sloop", "Yawl", "Ketch", "Reader", "JsonReader", "MarkdownReader" ], function(Sailor, SailorExpert, sloop, yawl, ketch, 
		Reader, JsonReader, MarkdownReader) {'use strict';

    describe("Simple Sailor Suite", function() {

        it("proves we can run a test", function() {
            expect(true).toBe(true);
        });

        it("creates a sloop and checks its name", function() {
            var boatType = sloop.getBoatType();
            expect(boatType).toBe("Sloop");
        });

        it("creates a Yawl and checks its name", function() {
            var boatType = yawl.getBoatType();
            expect(boatType).toBe("Yawl");
        });

        it("creates a Ketch and checks its name", function() {
            var boatType = ketch.getBoatType();
            expect(boatType).toBe("Ketch");
        });

        it("shows that a sailor can tack", function() {
            var sailor = new Sailor(sloop);
            var result = sailor.tack();
            expect(result).toBe('Sloop tack called.');
        });
        
         it("shows that a sailor expert can tack", function() {
            var sailor = new SailorExpert(sloop);
            var result = sailor.tack();
            expect(result).toBe('Sloop tack called.');
        });
        
        
        it("shows that a sailor expert can tack to port", function() {
            var sailor = new SailorExpert(sloop);
            var tackPort = sailor.tackPort();
            var currentTack = sailor.getCurrentTack();            
            expect(tackPort).toBe('Sloop tack called.');
            expect(currentTack).toBe("This Sloop is on the port tack.");
        });
        
		it("Test creation of Reader", function() {
            var reader = new Reader(sloop);
            expect( reader instanceof Reader).toBe(true);
        });
    });
    
    describe("Simple Reader Suite", function() {
    	it("Test creation of Reader", function() {
            var reader = new Reader(sloop);
            expect( reader instanceof Reader).toBe(true);
        });
        
 /*       it("Test creation of Reader", function() {
            var reader = new Reader(sloop);
            expect( reader instanceof Reader).toBe(true);
        });
 */       
 
        it("Test creation of Reader type Jason", function() {
            var reader = new Reader(JsonReader);
            expect( reader instanceof Reader).toBe(true);
        });

		it("Test creation of Reader type Jason and test getReaderType()", function() {
            var reader = new Reader(JsonReader);
            var value = reader.readerType.getReaderType();
            expect(value).toBe('JsonReader');
        });
        
        it("Test creation of Reader type Jason and test readFile()", function() {
            var reader = new Reader(JsonReader);
            var value = reader.readerType.readFile();
            expect(value[0].firstName).toBe('George');
            expect(value[2].lastName).toBe('Jefferson');
        });
        
        it("Test creation of Reader type MarkdownReader nd test getReaderType()", function() {
            var reader = new Reader(MarkdownReader);
            var value = reader.readerType.getReaderType();
            expect(value).toBe('MarkdownReader');
        });
        
        it("Test creation of Reader type MarkdownReader and test readFile()", function() {
            var reader = new Reader(MarkdownReader);
            var value = reader.readerType.readFile();
            expect(value).toBe('# My Markdown This is my markdown file. It has a list in it: - Item01 - Item02 - Item03');
        });
        
    });

});




