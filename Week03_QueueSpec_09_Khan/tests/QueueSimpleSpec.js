/**
 * @author Charlie Calvert
 */

describe("A Queue Simple Suite", function() {
	'use strict';
	var myQueue = new MyQueue;
	
	var qLength = 0;
	var queueElement;
	
	it("contains spec with an expectation", function() {
		expect(true).toBe(true);
	});

	it("Tests simple queue not to be null", function() {
		expect(myQueue).not.toBe(null);
	})
	
	it("Tests simple queue front element", function() {
		myQueue.enQueue("alpha");
		myQueue.enQueue("bravo");
		myQueue.enQueue("charlie");
		var value = myQueue.getFrontElement();
		expect(value).toBe("alpha");
		console.log(value); 
	});
	
	it("Tests simple queue Back element", function() {
		var value = myQueue.getBackElement();
		expect(value).toBe("charlie");
		console.log(value); // 
	});
	
	it("Tests simple queue Prove that calling getFrontElement does not change the length of the queue", function() {
		var value = myQueue.getQueueLength();
		expect(value).toBe(3);
		console.log(value); // 
	});
	
	it("Tests simple queue deQueue() and isEmpty()", function() {
		while (myQueue.getQueueLength() > 0){
			qLength++;
			myQueue.deQueue();
		};
		expect(qLength).toBe(3);
		console.log(value); 
		var value = myQueue.isEmpty();
		expect(value).toBe(true);
		console.log(value); 
	});
	

	
	it("Tests simple queue dequeue first returns alpha, then bravo then charlie", function() {
		myQueue.enQueue("alpha");
		myQueue.enQueue("bravo");
		myQueue.enQueue("charlie");
		
		var value;
		var x;
		
		for(x = 0; x < 3; x++){
			value = myQueue.deQueue();
			if(x == 0){
				expect(value).toBe("alpha");
				console.log(value);
			};
			if(x == 1){
				expect(value).toBe("bravo");
				console.log(value);
			};
			if(x == 2){
				expect(value).toBe("charlie");
				console.log(value);
			};
		};
	});
	
	it("Tests simple queue padNumber()", function() {
		var x;
		for(x=0; x<100000; x++){
			queueElement = myQueue.padNumber(x+1, 6, 0);
			myQueue.enQueue("Item" + queueElement);
		};
		expect(100000).toBe(myQueue.getQueueLength());
		console.log(myQueue.getQueueLength());  
	});
	

		it("Tests simple queue dequeue first returns Item000001, then Item000003 then Item000003", function() {
		var value;
		var x;
		
		for(x = 0; x < 3; x++){
			value = myQueue.deQueue();
			if(x == 0){
				expect(value).toBe("Item000001");
				console.log(value);
			};
			if(x == 1){
				expect(value).toBe("Item000002");
				console.log(value);
			};
			if(x == 2){
				expect(value).toBe("Item000003");
				console.log(value);
			};
		};
	});

});
