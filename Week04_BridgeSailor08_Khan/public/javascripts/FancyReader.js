/**
 * @author Mike Khan
 */


define(function(require) {'use strict';

    var FancyReader = (function() {
    
        function FancyReader(readerTypeObj) {
            this.setReader(readerTypeObj);
        }
        
        FancyReader.prototype.setReader = function(readerTypeObj) {
            this.readerType = readerTypeObj;
        };
        
        FancyReader.prototype.getLength = function(readerTypeObj) {
        	var value = this.readerType.readFile();
        	value = value.length;
            return value;
        };

        return FancyReader;
    }());

    return FancyReader;
});