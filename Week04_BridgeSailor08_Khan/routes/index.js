var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Week04 Bridge_Reader Khan' });
});

module.exports = router;
