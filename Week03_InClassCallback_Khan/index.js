/**
 * @author Charlie
 */

function hello(func) {
    'use strict';
    $("#test02").html("It works! ");
    func();
};

function retunValue(callback){
	return  callback();

};

$(document).ready(function() {
    "use strict";

    $("#test01").html("Document Ready called");
	
	var retValue = function() {
		return 327;
    };

    var callback = function() {
        $("#test03").html("It's a nine!");
    };
	
	hello(callback);
	
	$("#test04").html ("the value returned from callback function retValue = " + retunValue(retValue));
});
