var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

var serverText = "The server sent me";

app.get('/Item01', function(request, response) {
	console.log("in /Item01");
    var result = { "result": "Success from server Item01",
    				"route": "Route of /Item01",
    				"message": "The server sent me from Item01"
    };
    response.send(result);
});

app.get('/Item02', function(request, response) {
    var result = { "result": "Success from server Item02", 
    				"route": "route of /Item02",
    				"message": "The server sent me from Item02"
    };
    response.send(result);
});

app.get('/Item03', function(request, response) {
    var result = { "result": "Success from server /Item03", 
    				"route": "route of Item03",
    				"message": "The server sent me from Item03"
    };
    response.send(result);
});

app.use('/users', users);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
