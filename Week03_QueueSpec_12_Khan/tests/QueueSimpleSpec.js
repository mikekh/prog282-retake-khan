/**
 * @author Charlie Calvert
 */

describe("A Queue Simple Suite", function() {
	'use strict';
	
	var qLength = 0;
	var queueElement;
	
	var myQueue = new MyQueue();
	//var myQueue = null;
	// This method is called before each test
/*    beforeEach(function() {
        myQueue = new MyQueue();
    });
*/
	it("contains spec with an expectation", function() {
		expect(true).toBe(true);
	});

	it("Tests simple queue not to be null", function() {
		expect(myQueue).not.toBe(null);
	})
	
	it("Tests simple queue to be empty", function() {
		var value = myQueue.getQueueLength();
		expect(value).toBe(0);
		console.log(value); 
	});
	
	it("Tests simple queue front element", function() {
		myQueue.enQueue("alpha");
		myQueue.enQueue("bravo");
		myQueue.enQueue("charlie");
		var value = myQueue.getFrontElement();
		expect(value).toBe("alpha");
		console.log(value); 
	});
	
	it("Tests simple queue Back element", function() {
		var value = myQueue.getBackElement();
		expect(value).toBe("charlie");
		console.log(value); // 
	});
	
	it("Tests simple queue Prove that calling getFrontElement does not change the length of the queue", function() {
		var value = myQueue.getQueueLength();
		expect(value).toBe(3);
		console.log(value); 
	});
	
	it("Tests simple queue deQueue() and isEmpty()", function() {
		while (myQueue.getQueueLength() > 0){
			qLength++;
			myQueue.deQueue();
		};
		expect(qLength).toBe(3);
		console.log(value); 
		var value = myQueue.isEmpty();
		expect(value).toBe(true);
		console.log(value); 
	});
	

	
	it("Tests simple queue dequeue first returns alpha, then bravo then charlie", function() {
		myQueue.enQueue("alpha");
		myQueue.enQueue("bravo");
		myQueue.enQueue("charlie");
		
		var value;
		var x;
		
		for(x = 0; x < 3; x++){
			value = myQueue.deQueue();
			if(x == 0){
				expect(value).toBe("alpha");
				console.log(value);
			};
			if(x == 1){
				expect(value).toBe("bravo");
				console.log(value);
			};
			if(x == 2){
				expect(value).toBe("charlie");
				console.log(value);
			};
		};
	});
	
	it("Tests simple queue padNumber()", function() {
		var x;
		for(x=0; x<100000; x++){
			queueElement = myQueue.padNumber(x+1, 6, 0);
			myQueue.enQueue("Item" + queueElement);
		};
		expect(100000).toBe(myQueue.getQueueLength());
		console.log(myQueue.getQueueLength());  
	});
	

		it("Tests simple queue dequeue first returns Item000001, then Item000003 then Item000003", function() {
		var value;
		var x;
		
		for(x = 0; x < 3; x++){
			value = myQueue.deQueue();
			if(x == 0){
				expect(value).toBe("Item000001");
				console.log(value);
			};
			if(x == 1){
				expect(value).toBe("Item000002");
				console.log(value);
			};
			if(x == 2){
				expect(value).toBe("Item000003");
				console.log(value);
			};
		};
	});

});

describe("A Simple Stack Suite", function() {
	'use strict';
	var value;
	var stackLength = 0;
	var stackElement;
	
	//var myStack = new MyStack();
	var myStack = null;
	// This method is called before each test
    beforeEach(function() {
        myStack = new MyStack();
    });


	it("Tests Simple Stack not to be null", function() {
		expect(myStack).not.toBe(null);
	});
	
	it("Tests Simple Stack number if item", function() {
		myStack.pushStack("alpha");
		myStack.pushStack("bravo");
		myStack.pushStack("charlie");
		
		value = myStack.getStackLength();
		expect(value).toBe(3);
		console.log(value); 
	});
	
	it("Tests Simple Stack value of pop", function() {
		myStack.pushStack("alpha");
		myStack.pushStack("bravo");
		myStack.pushStack("charlie");
		
		value = myStack.popStack();
		expect(value).toBe("charlie");
		console.log(value); 
	});
	


	it("Tests Simple Stack value of testPalindrome()2300320", function() {
		value = myStack.testPalindrome(230032);
		expect(value).toBe(true);
	});
	
	it("Tests Simple Stack value of testPalindrome()230012", function() {
		value = myStack.testPalindrome(230012);
		expect(value).toBe(false);
	});
	
	it("Tests Simple Stack value of testPalindrome()Was it a cat I saw?", function() {
		value = myStack.testPalindrome("Was it a cat I saw");
		expect(value).toBe(true);
	});
	
	it("Tests Simple Stack value of testPalindrome()Was it a bat or a rat that I saw", function() {
		value = myStack.testPalindrome("Was it a bat or a rat that I saw?");
		expect(value).not.toBe(true);
	});
	
	it("Tests Simple Stack value of testPalindrome() Able was I ere I saw Elba", function() {
		value = myStack.testPalindrome("Able was I ere I saw Elba");
		expect(value).toBe(true);
	});
	
	it("Tests Simple Stack removeItem() remove from middle", function() {
		initStack();
		value = myStack.removeItem("bravo");
		expect(value).toBe(true);
	});
	
	it("Tests Simple Stack removeItem() remove from front", function() {
		initStack();
		value = myStack.removeItem("alpha");
		expect(value).toBe(false);
	});
	
	it("Tests Simple Stack removeItem() remove from back", function() {
		initStack();
		value = myStack.removeItem("echo");
		expect(value).not.toBe(true);
	});
	
	var initStack = function(){
		myStack.pushStack("alpha");
		myStack.pushStack("bravo");
		myStack.pushStack("charlie");
		myStack.pushStack("delta");
		myStack.pushStack("echo");
	};
	
	//Charlie I don't understand how this works. Are we just forcing an exception. I have add a push just to see if it behaves any differently, but it does not
	 it("shows Simple Stack can't not pop a empty stack", function() {
            //myStack.pushStack("alpha");
			expect(function() { myStack.popStack(); }).toThrow(new TypeError('Cannot pop empty stack.'));
        });
	
});

