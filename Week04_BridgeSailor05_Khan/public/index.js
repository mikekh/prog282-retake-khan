

function parse() {
    'use strict';
	//var jsonData = [];
	var jsonData;
	
    $.getJSON('/readJson', function(data) {

        //jsonData = JSON.stringify(data);
        jsonData = data;
    })
        .success(function() {
            console.log("csc: success. Loaded index.json");
            //clear nameDiv
            $("#nameDiv").html("");
            $("#nameDiv").append(jsonData);
            
            //loop and display each item in json file
            $.each(jsonData, function(i, item) {
            	$("#nameDiv").append(item.firstName + ' ' + item.lastName + '<br/>');
        	});
        })
        .error(function(jqXHR, textStatus, errorThrown) {
            alert("error calling JSON. Try JSONLint or JSLint: " + textStatus + errorThrown);
        })
        .complete(function() {
            console.log("csc: completed call to get index.json");
        });
}

function mdRead() {
    'use strict';
	var rtData;
	
    $.get('/readMD', function(data) {
        rtData = data;
    })
        .success(function() {
            console.log("csc: success. Loaded index.json");
            //clear nameDiv
            $("#nameDiv").html("");
            $("#nameDiv").html('<br/>' + "Charlie! cant get it display with the new line " + '<br/>' + '<br/>');
            $("#nameDiv").append(rtData);
        })
        .error(function(jqXHR, textStatus, errorThrown) {
            alert("error calling JSON. Try JSONLint or JSLint: " + textStatus + errorThrown);
        })
        .complete(function() {
            console.log("csc: completed call to get index.json");
        });
}



$(document).ready(function() {
    'use strict';
    $("#btnJson").click(parse);
    $("#btnMD").click(mdRead);
});
