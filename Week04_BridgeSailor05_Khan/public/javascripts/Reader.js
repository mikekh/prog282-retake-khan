/**
 * @author Mike Khan
 */

define(function(require) {'use strict';

    var Reader = (function() {
    
        function Reader(readerTypeObj) {
            this.setReader(readerTypeObj);
        }
        
        Reader.prototype.setReader = function(readerTypeObj) {
            this.readerType = readerTypeObj;
        };

        return Reader;
    }());

    return Reader;
}); 